#pragma once

#define MAP_AH	17
#define MAP_AW	96


class Mapchip
{
private:


public:

	int mapdata[MAP_AH][MAP_AW] = { 0 };	//今のマップデータ
	int chipsize = 0;					//チップサイズ



	Mapchip() {}
	~Mapchip() {}

	void Initialize();
	void Update();
	void Draw();
	

};