#pragma once
#include <iostream>
#include <d3d11.h>
#include"sprite.h"
#include<stdio.h>
#include<string.h>
#include"scene_manager.h"
#include"sound.h"
class Ranking
{
private:
    //リザルト背景アニメーション
    int Count;
    int ACount;
    //ハイスコアの時何秒間だけだす
    float timer;
    std::unique_ptr<Sprite>Line_A;

    //ハイスコアの時に出す画像
    float cnt;
    float sizer;
    float sizer_x;
    float sizer_y;
    float pos_x;
    float pos_y;
    std::unique_ptr<Sprite> hight_S;
    std::unique_ptr<Sound>hight_scene;
public:
    //それぞれの画像の実体
    std::unique_ptr<Sprite> sprites;
    std::unique_ptr<Sprite> Result;
    int y; //高さ
    float x;//横
    float size;//サイズ

               //ランキング
    int rank[5] = { 5000,4000,6000,9000,1000 };//スコアを保存する用
    int time;//時間
    int left = 0;//クイックソート用の配列
    int ligh = 4;//クイックソート用の配列
                 //桁保存
    int _digit[100][100];//桁保存用の配列
    int add = 0; //配列を進める
                 //桁調べる
    int digit_[100];//桁数を調べる
    int digit_Score[100];//桁の保存用配列
    int nowtime;//現在の時間
   
 


    int Select();
    bool Initialize(ID3D11Device *Device);
    void Rank_read();//ファイルの読み込み
    void Rank_write();//書き込み
    void Rank_save();//セーブ（使わない）
    int  Update(float);
    void digit();
    void Render(ID3D11DeviceContext* immediate_context, float elapsed_time);
    //quick_sort
    void swap(int *x, int *y);//値入れ替え関数
    int partition(int array[], int left, int right);//値を区切る
    void quick_sort(int array[], int left, int right);//ソート


    static Ranking& getInstance()
    {
        static Ranking  instance;
        return	instance;
    }
};
#define	pRanking Ranking::getInstance()
