#include "Shot.h"
#include "fbx_load.h"

Shot::Shot(ID3D11Device* device) {
    pos = DirectX::XMFLOAT3(0, 0, 0);
    angle = DirectX::XMFLOAT3(0, 0, 0);
    scale = DirectX::XMFLOAT3(1, 1, 1);
    color = DirectX::XMFLOAT4(0, 0, 1, 1);
    //モデルの読み込み
    const char*filename = "Data/FBX/000_cube.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    obj_Render = std::make_shared<ModelRenderer>(device);
    exist = false;
}
void Shot::init() {
    exist = true;
}
void Shot::Update(DirectX::XMFLOAT3 p) {
    pos.x += p.x;
    pos.y += p.y;
    pos.z += p.z;
    exist = true;
}
void Shot::Render(ID3D11DeviceContext* context,
    float elapsed_time, const DirectX::XMFLOAT4X4 &view,
    const DirectX::XMFLOAT4X4 &prolection, const DirectX::XMFLOAT4&light_direction)
{
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {

        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
        t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//移動行列

        W = s*r*t;
    }

    //worldviewprolectionの作成
    DirectX::XMFLOAT4X4 WVP;
    {
        DirectX::XMMATRIX wvp;
        DirectX::XMMATRIX v = DirectX::XMLoadFloat4x4(&view);
        DirectX::XMMATRIX p = DirectX::XMLoadFloat4x4(&prolection);
        wvp = W*v*p;

        XMStoreFloat4x4(&WVP, wvp);
    };
    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(W);

    obj_Render->Begin(context, WVP, light_direction);
    obj_Render->Render(context, *obj, DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f));
    obj_Render->End(context);
}