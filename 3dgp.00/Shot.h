#include <memory.h>
#include "model.h"
#include "model_renderer.h"


class Shot {
private:
    std::shared_ptr<Model> obj;
    std::shared_ptr<ModelRenderer> obj_Render;

    DirectX::XMFLOAT3 pos;
    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT2 speed;

    
public:
    bool exist;
    Shot(ID3D11Device* device);
    void init();
    void Update(DirectX::XMFLOAT3 pos);
    void Render(ID3D11DeviceContext* context, float elapsed_time, const DirectX::XMFLOAT4X4 &view, const DirectX::XMFLOAT4X4 &prolection, const DirectX::XMFLOAT4&light_direction);
    static Shot&getinctanse()
    {
        static Shot*shot;
        return *shot;
    }
};
#define	pShot Shot::getInstance()