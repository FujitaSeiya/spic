#pragma once
#include"scene.h"
#include"sprite.h"
#include<memory>
#include <thread>
#include <mutex>
class Scene_Game :public Scene
{
private:
    DirectX::XMFLOAT4 light;



public:
    std::unique_ptr<std::thread> loading_thread;
    std::mutex loading_mutex;

    bool IsNowLoading()
    {
        if (loading_thread && loading_mutex.try_lock())
        {
            loading_mutex.unlock();
            return false;
        }
        return true;
    }
    void EndLoading()
    {
        if (loading_thread && loading_thread->joinable())
        {
            loading_thread->join();
        }
    }

    Scene_Game(ID3D11Device*device);
    void Load_leng();
    void Save_leng();
    int Update(float elapsed_time);
    void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
    ~Scene_Game();
};
