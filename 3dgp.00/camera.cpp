#include "camera.h"
Camera*Camera::m_camera = nullptr;

Camera::Camera(const DirectX::XMFLOAT3& eye, const DirectX::XMFLOAT3& focus, const DirectX::XMFLOAT3& up)
{
	m_eye = eye;
	m_focus = focus;
	m_up = up;
	m_type = 0;
}

void Camera::SetCamera(const DirectX::XMFLOAT3 & target_pos, const DirectX::XMFLOAT3 & leng, const DirectX::XMFLOAT3 & target_angle, const Camera_TYPE & type)
{
	m_target_pos = target_pos;
	m_leng = leng;
	m_target_angle = target_angle;
	m_type = static_cast<int>(type);
	m_up = DirectX::XMFLOAT3(0.f, 1.f, 0.f);
}

void Camera::Updata()
{
	switch (m_type)
	{
	case Camera_TYPE::RELATIVE_POS:
		Relative_pos_Camera();
		break;
	case Camera_TYPE::TRACKING:

		break;
	case Camera_TYPE::TPS_CAMERA:

		break;
	case Camera_TYPE::FPS_CAMERA:

		break;
	}
	m_eye.z *= -1.f;
	m_focus.z *= -1.f;
}

void Camera::Relative_pos_Camera()
{
#if 0
	static float angle = DirectX::XMConvertToRadians(45.f)/*atan2f(player.pos.x - p.x, player.pos.z - p.z)*/, dangle = DirectX::XMConvertToRadians(1.0f);
	//static float renge = sqrtf(64.f + 64.f);
	if (GetAsyncKeyState('A') < 0) {
		angle -= dangle;
	}
	if (GetAsyncKeyState('D') < 0) {
		angle += dangle;
	}
	m_eye = { m_target_pos.x + (sinf(angle)*m_leng.x),m_target_pos.y + m_leng.y,m_target_pos.z + (cosf(angle)*m_leng.z) };

	m_focus = m_target_pos;
#else
	m_eye = DirectX::XMFLOAT3(m_target_pos.x + m_leng.x, m_target_pos.y + m_leng.y, m_target_pos.z + m_leng.z);
	m_focus = m_target_pos;

#endif
}

