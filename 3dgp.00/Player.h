#pragma once
#include <memory>
#include "model.h"
#include "model_renderer.h"
#include "Shot.h"
class Player {
private:
    std::shared_ptr<Model> obj;
    std::shared_ptr<ModelRenderer> obj_Render;
    std::unique_ptr<Shot> shot;

    DirectX::XMFLOAT3 angle;
    DirectX::XMFLOAT3 scale;
    DirectX::XMFLOAT4 color;
    DirectX::XMFLOAT2 speed;
    DirectX::XMFLOAT3 pos;
    bool exist;

    enum TYPE {
        SType,
        NType,
        NUM,
    };
    int Type = TYPE::SType;
    POINT pt;

public:

    Player(ID3D11Device* device);
    
    void Init(ID3D11Device* device);
    void Update(ID3D11Device* device);
    void PL_Shot(ID3D11Device* device);
    void Render(ID3D11DeviceContext* context, 
        float elapsed_time, const DirectX::XMFLOAT4X4 &view,
        const DirectX::XMFLOAT4X4 &prolection,
        const DirectX::XMFLOAT4&light_direction);

 
     
    static Player&getinctanse()
    {
        static Player*player;
        return *player;
    }
};

#define	pPlayer Player::getInstance()
