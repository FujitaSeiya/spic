#include "Scene_game.h"
#include "camera.h"
#include <DirectXMath.h>


Scene_Game::Scene_Game(ID3D11Device * device)
{


    light = DirectX::XMFLOAT4(0.f, -1, 0.5f, 0);
 
    pCamera.Create(DirectX::XMFLOAT3(10.0f, 50.0f, -500.0f), DirectX::XMFLOAT3(0.0f, 50.0f, 0.0f), DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f));
   // Load_leng();
}

int Scene_Game::Update(float elapsed_time)
{
  

    return 0;
}




void Scene_Game::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
    const float PI = 3.14f;
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {
        static float rz = 0;
        //rz += 0.2f*(PI / 180.f);
        static float rt = (PI / 180.f)*45.f;
        //rt += 0.2f*(PI / 180.f);
        //Z軸方向に前後移動してみる
        static float mz = 0;
        //mz += 0.002f;
        float tz = sinf(mz)*5.0f;

        DirectX::XMFLOAT3 scale(1, 1, 1);
        DirectX::XMFLOAT3 rotate(0, 0, rz);
        DirectX::XMFLOAT3 translate(0, 0, 0);

        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(rotate.x, rotate.y, rotate.z);//回転行列
        t = DirectX::XMMatrixTranslation(translate.x, translate.y, translate.z);//移動行列

        W = s*r*t;
    }
    //ビュー行列
    DirectX::XMMATRIX V;
    {
        //カメラの設定
        DirectX::XMVECTOR eye, focus, up;
        eye = DirectX::XMVectorSet(0.0f, 50.0f, -50.0f, 1.0f);//視点
        focus = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);//注視点
        up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);//上ベクトル

        V = DirectX::XMMatrixLookAtLH(eye, focus, up);
    }
    DirectX::XMMATRIX P;
    {
        //画面サイズ取得のためビューポートを取得
        D3D11_VIEWPORT viewport;
        UINT num_viewports = 1;
        devicecontext->RSGetViewports(&num_viewports, &viewport);

        //角度をラジアン(θ)に変換
        float fov_y = 30 * (PI / 180.f);//角度
        float aspect = viewport.Width / viewport.Height;//画面比率
        float near_z = 0.1f;//表示最近面までの距離
        float far_z = 100000.0f;//表紙最遠面までの距離

        P = DirectX::XMMatrixPerspectiveFovLH(fov_y, aspect, near_z, far_z);
    }
    //ビュープロジェクション行列
    DirectX::XMFLOAT4X4 world_view_projection;
    {
        DirectX::XMMATRIX WVP;
        DirectX::XMMATRIX C = DirectX::XMMatrixSet(
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, -1, 0,
            0, 0, 0, 1
            );

        WVP = C*V*P;
        DirectX::XMStoreFloat4x4(&world_view_projection, WVP);
    }
    //ビュー行列、プロジェクション行列を変換
    DirectX::XMFLOAT4X4 view, projiution;
    DirectX::XMStoreFloat4x4(&view, V);
    DirectX::XMStoreFloat4x4(&projiution, P);
}

Scene_Game::~Scene_Game()
{
    pCamera.Destroy();
}
