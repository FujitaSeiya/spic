#include "Player.h"
#include "fbx_load.h"
#include <Windows.h>

Player::Player(ID3D11Device* device) {
//パラメータ（仮）※後で修正する
    pos = DirectX::XMFLOAT3(0, 0, 0);
    angle = DirectX::XMFLOAT3(5, 0, 0);
    scale = DirectX::XMFLOAT3(1, 1, 1);
    color = DirectX::XMFLOAT4(0, 0, 1, 1);
    //モデルの読み込み
    const char*filename = "Data/FBX/machine_girl.fbx";
    std::unique_ptr<ModelData> model_data = std::make_unique<ModelData>();
    FbxLoader fbx_loader;
    fbx_loader.Load(filename, *model_data);
    std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));
    obj = std::make_shared<Model>(model_resource);

    obj_Render = std::make_shared<ModelRenderer>(device);
    //マウスの座標取得
    GetCursorPos(&pt);

    shot = std::make_unique<Shot>(device);
    exist = false;
}


void Player::Update(ID3D11Device* device) {
    const float speed = 0.05f;
    DirectX::XMFLOAT2 dist;
    dist = DirectX::XMFLOAT2(sqrtf(pt.x - pos.x), sqrtf(pt.y - pos.z));
    //前進後進
    if (GetAsyncKeyState('W') < 0)
    {
        pos.z += speed;
    }
    if (GetAsyncKeyState('S') < 0)
    {
        pos.z -= speed;
    }
    //左右移動
    if (GetAsyncKeyState('A') < 0) {
        pos.x -= speed;
    }
    if (GetAsyncKeyState('D') < 0) {
        pos.x += speed;
    }
    if (GetAsyncKeyState(VK_SPACE) & 1) {
        Type = (Type + 1) % TYPE::NUM;
    }
    if (GetAsyncKeyState(VK_LBUTTON) & 1) {
        shot->exist = true;

       
        
    }
     if(shot->exist)
        PL_Shot(device);
    angle.x = atan2(dist.x, dist.x);
}

void Player::PL_Shot(ID3D11Device* device) {
    DirectX::XMFLOAT3 color2;
    switch (Type) {
    case TYPE::SType:
        color2 = DirectX::XMFLOAT3(0, 0, 1);
        break;
    case TYPE::NType:
        color2 = DirectX::XMFLOAT3(1, 0, 0);
        break;
    }
    const float	SHOT_SPEED = 0.5f;
    const float OFS_FRONT = 0.45f;
    const float OFS_HEIGHT = 0.55f;

    DirectX::XMFLOAT3 p = pos;
    p.x += sinf(angle.x) * OFS_FRONT+SHOT_SPEED;
    p.z += cosf(angle.z) * OFS_FRONT+SHOT_SPEED;
    p.y += OFS_HEIGHT;
    shot->Update(p);
}
void Player::Render(ID3D11DeviceContext* context, 
    float elapsed_time, const DirectX::XMFLOAT4X4 &view,
    const DirectX::XMFLOAT4X4 &prolection, const DirectX::XMFLOAT4&light_direction)
{
    //ワールド行列を作成
    DirectX::XMMATRIX W;
    {

        DirectX::XMMATRIX s, r, t;
        s = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);//スケール行列
        r = DirectX::XMMatrixRotationRollPitchYaw(angle.x, angle.y, angle.z);//回転行列
        t = DirectX::XMMatrixTranslation(pos.x, pos.y, pos.z);//移動行列

        W = s*r*t;
    }

    //worldviewprolectionの作成
    DirectX::XMFLOAT4X4 WVP;
    {
        DirectX::XMMATRIX wvp;
        DirectX::XMMATRIX v = DirectX::XMLoadFloat4x4(&view);
        DirectX::XMMATRIX p = DirectX::XMLoadFloat4x4(&prolection);
        wvp = W*v*p;

        XMStoreFloat4x4(&WVP, wvp);
    };
    obj->CalculateLocalTransform();
    obj->CalculateWorldTransform(W);

    obj_Render->Begin(context, WVP, light_direction);
    obj_Render->Render(context, *obj, DirectX::XMFLOAT4(1.f, 1.f, 1.f, 1.f));
    if(shot->exist)
    shot->Render(context, elapsed_time, view, prolection, light_direction);
    obj_Render->End(context);
};
