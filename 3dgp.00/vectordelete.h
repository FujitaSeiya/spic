#pragma once
#include<vector>
#include<algorithm>
template<class T>
inline void Vectordelete(std::vector<T>&obj)
{
	if (obj.empty())return;
	auto RemoveCondition
	{
		[](auto& ob)
	{
		if (ob)
			return false;
		else
			return true;
	}
	};

	auto remove = std::remove_if(obj.begin(), obj.end(), RemoveCondition);
	obj.erase(remove, obj.end());
}