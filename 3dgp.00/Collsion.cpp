#include "Collsion.h"

//●と●
 bool Collision::HitCollision(DirectX::XMFLOAT3 ShotPos, float Sradius, DirectX::XMFLOAT3 Epos, float Eradius)
{
    if ((Epos.x - ShotPos.x)*(Epos.x - ShotPos.x) + (Epos.y - ShotPos.y)*(Epos.y - ShotPos.y) + (Epos.z - ShotPos.z)*(Epos.z - ShotPos.z)
        <= (Sradius + Eradius)*(Sradius + Eradius))
    {
        return true;

    }
    return false;

}

 int Collision::RayPick(const DirectX::XMFLOAT3 & startPosition, const DirectX::XMFLOAT3 & endPosition, DirectX::XMFLOAT3 * outPosition, DirectX::XMFLOAT3 * outNormal, float * outLenght)
 {
     using namespace DirectX;

     int ret = -1;
     XMVECTOR start = XMLoadFloat3(&startPosition);
     XMVECTOR end = XMLoadFloat3(&endPosition);
     XMVECTOR vec = XMVectorSubtract(end, start);
     XMVECTOR length = XMVector3Length(vec);
     XMVECTOR dir = XMVector3Normalize(vec);

     float neart;

     XMStoreFloat(&neart, length);

     XMVECTOR position, normal;
     for (const auto& it : faces)
     {
         //面頂点取得
         XMVECTOR a = DirectX::XMLoadFloat3(&it.position[0]);
         XMVECTOR b = DirectX::XMLoadFloat3(&it.position[1]);
         XMVECTOR c = DirectX::XMLoadFloat3(&it.position[2]);
         //  3 辺算出 
         XMVECTOR ab = XMVectorSubtract(b, a);
         XMVECTOR bc = XMVectorSubtract(c, b);
         XMVECTOR ca = XMVectorSubtract(a, c);
         // 外積による法線算出 
         XMVECTOR n = XMVector3Cross(ab, bc);
         // 内積の結果がプラスならば裏向き 
         XMVECTOR dot = XMVector3Dot(dir, n);
         float fdot = 0;
         XMStoreFloat(&fdot, dot);
         if (fdot >= 0) continue;
         // 交点算出 
         XMVECTOR as = XMVectorSubtract(a, start);
         XMVECTOR d1 = XMVector3Dot(as, n);
         XMVECTOR d2 = XMVector3Dot(dir, n);
         XMVECTOR x = XMVectorDivide(d1, d2);
         XMVECTOR vx = XMVectorMultiply(dir, x);
         XMVECTOR cp = XMVectorAdd(start, vx);
         //  内点判定
         float fwork;
         XMVECTOR v1 = XMVectorSubtract(a, cp);
         XMVECTOR temp = XMVector3Cross(v1, ab);
         XMVECTOR work = XMVector3Dot(temp, n);
         DirectX::XMStoreFloat(&fwork, work);
         if (fwork < 0.0f) continue;

         XMVECTOR v2 = XMVectorSubtract(b, cp);
         XMVECTOR temp2 = XMVector3Cross(v2, bc);
         XMVECTOR work2 = XMVector3Dot(temp2, n);
         DirectX::XMStoreFloat(&fwork, work2);
         if (fwork < 0.0f) continue;

         XMVECTOR v3 = XMVectorSubtract(c, cp);
         XMVECTOR temp3 = XMVector3Cross(v3, ca);
         XMVECTOR work3 = XMVector3Dot(temp3, n);
         DirectX::XMStoreFloat(&fwork, work3);
         if (fwork < 0.0f) continue;
         // 情報保存 
         float ft = 0;
         DirectX::XMStoreFloat(&ft, x);
         neart = ft;
         position = cp;
         normal = n;
         ret = it.materialIndex;
     }
     if (ret != -1)
     {
         XMStoreFloat3(outPosition, position);
         XMStoreFloat3(outNormal, normal);
     }
     *outLenght = neart;
     return ret;

 }
