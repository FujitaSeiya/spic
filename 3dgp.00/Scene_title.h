#pragma once
#include "scene.h"
#include"sprite.h"

class Scene_Title :public Scene
{
private:
	std::unique_ptr<Sprite> sample;

public:
    Scene_Title(ID3D11Device*device);
    int Update(float elapsed_time);
    void Render(float elapsed_time, ID3D11DeviceContext* devicecontext);
    ~Scene_Title();
};