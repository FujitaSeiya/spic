#include "Scene_Result.h"
#include"camera.h"
#include "Ranking.h"

Scene_Result::Scene_Result(ID3D11Device *device)
{
    pRanking.Initialize(device);
}

int Scene_Result::Update(float elapsed_time)
{
    return 0;
}

void Scene_Result::Render(float elapsed_time, ID3D11DeviceContext*devicecontext)
{
    pRanking.Render(devicecontext, elapsed_time);
}
Scene_Result::~Scene_Result()
{

}