#include "Scene_title.h"
#include "camera.h"
#include "keyboad.h"

#include <imgui.h>

#include <imgui_impl_dx11.h>
#include <imgui_impl_win32.h>
#include <imgui_internal.h>
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

Scene_Title::Scene_Title(ID3D11Device * device)
{
	sample = std::make_unique<Sprite>(device,L"Data//image//Sample.png");
}

int Scene_Title::Update(float elapsed_time)
{
	ImGui::Begin("Start");

	ImGui::End();
    return 0;
}
void Scene_Title::Render(float elapsed_time, ID3D11DeviceContext* devicecontext)
{
	//sample->render(devicecontext, 32, 32, 96, 96, 0, 0, 64, 64, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
	sample->SetColor(0.5f, 0.58f, 0.5f);
	sample->render(devicecontext, 0, 0, 1.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f);
}

Scene_Title::~Scene_Title()
{
   
}