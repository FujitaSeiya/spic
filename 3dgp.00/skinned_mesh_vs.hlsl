//float4 main( float4 pos : POSITION ) : SV_POSITION
//{
//	return pos;
//}
#include "skinned_mesh.hlsli"
VS_OUT main(float4 pos : POSITION, float3 normal : NORMAL, float2 texcoord : TEXCOORD, float4 bone_weights : WEIGHTS, uint4  bone_indices : BONES)
{
	
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	for (int j = 0; j < 4; j++)
	{

		p += (bone_weights[j] * mul(pos, bone_transforms[bone_indices[j]])).xyz;
		n += (bone_weights[j] * mul(float4(normal.xyz, 0), bone_transforms[bone_indices[j]])).xyz;
	}
	pos = float4(p, 1.0f);
	normal = float4(n, 0.0f);
	VS_OUT vout;
	vout.pos = mul(pos, world_view_projection);
	float3 N = normalize(mul(normal, (float3x3)world));
	float3 L = normalize(-light_direction.xyz);
	float d = dot(L, N);
	float4 influence = { 0, 0, 0, 1 };
	int count = 0;
	for (int i = 0; i < 4; i++)
	{
		float weight = bone_weights[i];
		if (weight > 0.0f)
		{
			switch (bone_indices[i])
			{
			case 0: influence.r = weight; break;
			case 1: influence.g = weight; break;
			case 2: influence.b = weight; break;
			}
		}
		else count++;
	}
	if (count < 3)vout.color = influence;
	else
	{
		vout.color.xyz = material_color.xyz*max(0, d);
		vout.color.w = material_color.w;
	}
	vout.texcoord = texcoord;
	return vout;
	



}
